import os.path
from os import path
import numpy as np
from numpy import loadtxt
import json

def get_one_frame(file_path, frame):
     with open(file_path) as json_file:
        y_label = json.load(json_file)
        y_arry = np.array(y_label)
        return y_arry

#print(get_one_frame("badboy", 1000))

def get_frames(vedio_name, start_frame = 0):
    frame = start_frame
    result = []
    while True:
        #print(frame)
        file_path = "../../data/" + vedio_name + "/label/frame" + str(frame) + ".txt"
        if(not path.exists(file_path)): break
        one_frame = get_one_frame(file_path, frame)
        #print(one_frame.shape)
        if(one_frame.shape != (1, 25, 3)):
            # pos not found return a zero matrix with correct size
            result.append(np.zeros((25, 2)))
        # drop the probability.
        else: result.append(one_frame[0,:,:-1])
        frame = frame + 1
    return result

#print(get_frames("badboy"))

def get_beats_np_array(vedio_name, start_frame = 0):
    with open("../../data/" + vedio_name + "/beats_output_vector.json") as json_file:
        y_label = json.load(json_file)
        y_arry = np.array(y_label[start_frame:])
        return y_arry

def get_feature_np_array(vedio_name, start_frame = 0):
    with open("../../data/" + vedio_name + "/feature_output_vector.json") as json_file:
        y_label = json.load(json_file)
        y_arry = np.array(y_label[start_frame:])
        return y_arry
